//
//  ViewController.swift
//  POP
//
//  Created by Svitlana Moiseyenko on 7/25/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  class UIImagePage: UIImageView, NotebookDisplayable {
  }
  
  @IBOutlet weak var imageView: UIImagePage!
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
     view.backgroundColor = UIColor.whiteColor()
     imageView.backgroundColor = UIColor(named: UIColor.IOSColor.Green)
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    imageView.displayAsNotebook(view)
    imageView.displayWithPages(5)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  
}


