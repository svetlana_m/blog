//
//  NotebookDisplayable.swift
//  POP
//
//  Created by Svitlana Moiseyenko on 7/25/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation
import UIKit


protocol NotebookDisplayable: class {}

extension NotebookDisplayable where Self: UIView {
  
 
  /*
  Display View like Notebook looks like
   
  :param: superview
  :param: cornerSize
  :param: corners
  */
  func displayAsNotebook(
    superview: UIView,
    cornerSize: CGSize = CGSizeMake(10, 10),
    corners: UIRectCorner = [.TopRight, .BottomRight]
    ) {
    
    // Do not continue if we already have such
    let shadowLayerName = "NotebookShadowLayer"
    for l in superview.layer.sublayers ?? [] {
      if l.name == shadowLayerName {
        return
      }
    }
    
    // Add round corners
    let cornerPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: cornerSize)
    let cornerLayer = CAShapeLayer()
    cornerLayer.frame = bounds
    cornerLayer.shouldRasterize = true
    cornerLayer.rasterizationScale = UIScreen.mainScreen().scale
    cornerLayer.path = cornerPath.CGPath
    layer.mask = cornerLayer
  }
  
  /*
   Display View with pages
   
   :param: numberOfPages
   :param: cornerSize
   :param: corners
   :param: pageFillColor
   :param: shadowOpacity
   :param: shadowRadius
   :param: shadowOffset
   :param: shadowOpacityLastPage
   :param: shadowRadiusLastPage
    :param: shadowOffsetLastPage
   */
  func displayWithPages(
    numberOfPages: Int = 1,
    cornerSize: CGSize = CGSizeMake(10, 10),
    corners: UIRectCorner = [.TopRight, .BottomRight],
    pageFillColor: CGColor = UIColor.whiteColor().CGColor,
    shadowOpacity: Float = 0.9,
    shadowRadius: CGFloat = 1.5,
    shadowOffset: CGSize = CGSizeMake(0, 0.8),
    shadowOpacityLastPage: Float = 0.5,
    shadowRadiusLastPage: CGFloat = 1.5,
    shadowOffsetLastPage: CGSize = CGSizeMake(0.0, 3)
    ) {
    
    let shapeLayerName = "PageShadowLayer"
    for l in superview?.layer.sublayers ?? [] {
      if l.name == shapeLayerName {
        return
      }
    }
    
    
    for index in (1..<numberOfPages + 1).reverse() {
      let isLastPage = index == numberOfPages
      
      // Calculate page offset and size
      var rect = bounds
      let indexFloat = CGFloat(index)
      rect.origin.y = bounds.origin.y + indexFloat / 3
      rect.origin.x = bounds.origin.x + indexFloat / 3
      rect.size.height = bounds.size.height + indexFloat / 2
      rect.size.width = bounds.size.width + indexFloat
      let pagePath = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: cornerSize)
      
      // Add page and shadow accordingly
      let shapeLayer = CAShapeLayer()
      shapeLayer.name = shapeLayerName
      shapeLayer.path = pagePath.CGPath
      shapeLayer.fillColor = isLastPage ? CGColorCreateCopyWithAlpha(pageFillColor, 0.7) : pageFillColor
      shapeLayer.shadowColor = UIColor.grayColor().CGColor
      shapeLayer.shadowOpacity = isLastPage ? shadowOpacityLastPage : shadowOpacity
      shapeLayer.shadowOffset = isLastPage ? shadowOffsetLastPage : shadowOffset
      shapeLayer.shadowRadius = isLastPage ? shadowRadiusLastPage : shadowRadius
      shapeLayer.frame = layer.frame
      shapeLayer.shouldRasterize = true
      shapeLayer.rasterizationScale = UIScreen.mainScreen().scale
      
      superview?.layer.insertSublayer(shapeLayer, below: layer)
    }
  }
}