//
//  PostTableCell.swift
//  Firebase_initial_setup
//
//  Created by Svitlana Moiseyenko on 8/5/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation
import UIKit

class PostTableCell: UITableViewCell {

  @IBOutlet weak var publishImageView: UIImageView!
  @IBOutlet weak var userImageView: UIImageView!
  
  @IBOutlet weak var postLabel: UILabel!  
  @IBOutlet weak var timeLabel: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib() 
    publishImageView.image = publishImageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
    publishImageView.tintColor = UIColor(named: UIColor.IOSColor.Orange)
  }
  
  var post: Post! {
    didSet {
      setContent()
    }
  }
  
  private func setContent() {
    postLabel.text = post.message
    timeLabel.text = DateUtils.timeAgoSinceDate(post.createdDate as! NSTimeInterval, numericDates: false)

    publishImageView.image = publishImageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
    publishImageView.tintColor = post.isPublished == 1 ? UIColor(named: UIColor.IOSColor.Orange) : UIColor(named: UIColor.IOSColor.Orange).colorWithAlphaComponent(0.3)
 
    let image = UIImage(named: "Avatar\(post.posterID).png")
    userImageView.image = image
  }
}