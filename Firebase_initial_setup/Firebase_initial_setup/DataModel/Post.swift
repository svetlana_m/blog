//
//  Post.swift
//  Firebase_initial_setup
//
//  Created by Svitlana Moiseyenko on 8/4/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation
import Firebase

//This enum reflects the Post’s collection structure.
enum PostKeys: String {
  case PostID      = "PostID"
  case PosterID    = "PosterID"
  case Message     = "Message"
  case CreatedDate = "CreatedDate"
  case IsPublished = "IsPublished"
}

class Post {

  var postID: String!    // Unique ID of Post
  var posterID: String!  // User Id
  let message: String!   // Message
  let createdDate: AnyObject! // The time when the post was created
  let isPublished: Int! // The published status of the post

  var ref: FIRDatabaseReference? // The direct reference to object in DB, so you can easy get an access to it

  
  // Initialize object from DB data
  init(snapshot: FIRDataSnapshot) {
    postID = snapshot.key
    posterID = snapshot.value![PostKeys.PosterID.rawValue] as! String
    message = snapshot.value![PostKeys.Message.rawValue] as! String
    createdDate = snapshot.value![PostKeys.CreatedDate.rawValue] as! NSTimeInterval
    isPublished = snapshot.value![PostKeys.IsPublished.rawValue] as! Int
    ref = snapshot.ref
  }
  
  // Initialize object for pushing to DB
  init (message: String) {
    
    self.postID = nil
    self.posterID = String(arc4random_uniform(5))
    self.message = message
    self.createdDate = FIRServerValue.timestamp()
    self.isPublished = 0
    self.ref = nil
  }
  
  // To covert object 
  func toAnyObject() -> AnyObject {
    return [
      PostKeys.PosterID.rawValue: posterID,
      PostKeys.Message.rawValue: message,
      PostKeys.CreatedDate.rawValue: createdDate,
      PostKeys.IsPublished.rawValue: isPublished
    ]
  }

 
  
}