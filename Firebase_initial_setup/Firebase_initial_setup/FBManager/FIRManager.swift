//
//  FIRManager.swift
//  Firebase_initial_setup
//
//  Created by Svitlana Moiseyenko on 8/3/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation
import Firebase
class FIRManager {
  
  var rootRef = FIRDatabaseReference.init()
  var postsRef = FIRDatabaseReference.init()
  
  static let sharedInstance = FIRManager()
  
  func initialize() -> FIRManager {
    rootRef = FIRDatabase.database().reference() //define the root ref of DB
    postsRef = rootRef.child("posts")            //define the ref of tablr post
    return self
  }

  
}