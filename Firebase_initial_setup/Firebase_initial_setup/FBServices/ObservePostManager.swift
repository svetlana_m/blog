//
//  ObservePostManager.swift
//  Firebase_initial_setup
//
//  Created by Svitlana Moiseyenko on 8/6/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation

/*
 This class serves the PostsService class and
 handles all possible errors and data result.
 */
class ObservePostManager {
  static let sharedInstance = ObservePostManager()
  
  // Inialize observing of all changes in Posts
  func initPostsObserve(completion: (postsArray: [Post]) -> Void) {
    PostsService.getPosts { postsArray in
      completion(postsArray: postsArray)
    }
  }
  
  // Creating the new post
  func createNewPost(message: String, completion: (post: Post?) -> Void) {
    guard message != "" else {
      completion(post: nil)
      return
    }
    
    PostsService.createNewPost(message) { post in
      completion(post: post)
    }
  }
  
  // Updating the existing post
  func updatePostReadStatus(post: Post?, isPublish: Int) {
    if post == nil {
        return
      }
    PostsService.publishPost(post, isPublish: isPublish)
  }

  // Removing the post
  func removePost(post: Post) {
    PostsService.removePost(post)
  }
  
}