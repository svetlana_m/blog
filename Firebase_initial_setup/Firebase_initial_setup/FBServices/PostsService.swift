//
//  PostsService.swift
//  Firebase_initial_setup
//
//  Created by Svitlana Moiseyenko on 8/3/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation
import Firebase

/*
 This class stores all necessary queries
 for working with Post DataModel
 */
class PostsService {
  
  class private weak var firManager : FIRManager? {
    get {
      return FIRManager.sharedInstance.initialize()
    }
  }
 
  // Get all posts
  class func getPosts(completion: (postsArray: [Post]) -> Void)  {
    firManager?.postsRef
      .observeEventType(.Value, withBlock: {
        snapshot in
        
        var localPosts = [Post]()
        for item in snapshot.children {
          let postItem = Post(snapshot: item as! FIRDataSnapshot)
          localPosts.append(postItem)
        }
        
        let reverseResults = localPosts.reverse() as [Post]
        completion(postsArray: reverseResults)
        
        }, withCancelBlock: { error in
          print(error.description)
          completion(postsArray: [Post]())
      })
  }
  
  // Creating new post
  class func createNewPost(message: String, completion: (post: Post?) -> Void) {
    let post = Post(message: message)
    firManager?.postsRef
      .childByAutoId()
      .setValue(post.toAnyObject()) { error, fb in
        
        if error == nil {
          post.postID = fb.key
          post.ref = fb
          completion(post: post)
        } else {
          completion(post: nil)
        }
    }
  }
  
  // Updating postStatus to Published/Un-Published
  class func publishPost(post: Post?, isPublish: Int) {
    
    //Btw, you shouldn’t use all fields, only which you need exactly
    let dic = [PostKeys.IsPublished.rawValue: isPublish]
    post?.ref!.updateChildValues(dic as [NSObject : AnyObject])
  }

  // Removing particular post
  class func removePost(post: Post) {
    post.ref?.removeValue()
  }
}