//
//  UIColor+App.swift
//  InvokeOneViewFromAnother
//
//  Created by Svitlana Moiseyenko on 6/30/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
 
  
  enum IOSColor : Int {
     case Yellow = 0xffcc00
     case Purple = 0x5856d6
     case Pink = 0xff2d55
     case Green = 0x4cd964
     case TealBlue = 0x5ac8fa
     case Blue = 0x007afa
     case Red = 0xff3b30
     case Orange = 0xff9500
  }
 
  
  convenience init(rgbValue: Int) {
    let red   = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
    let green = CGFloat((rgbValue & 0xFF00) >> 8) / 255.0
    let blue  = CGFloat(rgbValue & 0xFF) / 255.0
    
    self.init(red: red, green: green, blue: blue, alpha: 1.0)
  }
  
  convenience init(named name: IOSColor) {
    self.init(rgbValue: name.rawValue)
  }
}
