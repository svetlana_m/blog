//
//  DateUtils.swift
//  Firebase_initial_setup
//
//  Created by Svitlana Moiseyenko on 8/5/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation

final class DateUtils {
  
  // Converting the date to string
  class func dateToString(tiDate: NSTimeInterval) -> String {
    
    let date = NSDate(timeIntervalSince1970: tiDate/1000)
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "hh:mm"
    let dateString = dateFormatter.stringFromDate(date)
    return dateString
    
  }
  
  // Converting the date to string with calculating how much time passed from the current moment to particular timeinterval
  class func timeAgoSinceDate(timeinterval: NSTimeInterval, numericDates:Bool) -> String {
    
    let date = NSDate(timeIntervalSince1970: timeinterval/1000)
    let calendar = NSCalendar.currentCalendar()
    let now = NSDate()
    let earliest = now.earlierDate(date)
    let latest = (earliest == now) ? date : now
    let components:NSDateComponents = calendar.components([NSCalendarUnit.Minute , NSCalendarUnit.Hour , NSCalendarUnit.Day , NSCalendarUnit.WeekOfYear , NSCalendarUnit.Month , NSCalendarUnit.Year , NSCalendarUnit.Second], fromDate: earliest, toDate: latest, options: NSCalendarOptions())
    
    if (components.year >= 2) {
      return "\(components.year) years ago"
    } else if (components.year >= 1){
      if (numericDates){
        return "1 year ago"
      } else {
        return "Last year"
      }
    } else if (components.month >= 2) {
      return "\(components.month) months ago"
    } else if (components.month >= 1){
      if (numericDates){
        return "1 month ago"
      } else {
        return "Last month"
      }
    } else if (components.weekOfYear >= 2) {
      return "\(components.weekOfYear) weeks ago"
    } else if (components.weekOfYear >= 1){
      if (numericDates){
        return "1 week ago"
      } else {
        return "Last week"
      }
    } else if (components.day >= 2) {
      return "\(components.day) days ago"
    } else if (components.day >= 1){
      if (numericDates){
        return "1 day ago"
      } else {
        return "Yesterday"
      }
    } else if (components.hour >= 2) {
      return "\(components.hour) hours ago"
    } else if (components.hour >= 1){
      if (numericDates){
        return "1 hour ago"
      } else {
        return "An hour ago"
      }
    } else if (components.minute >= 2) {
      return "\(components.minute) minutes ago"
    } else if (components.minute >= 1){
      if (numericDates){
        return "1 minute ago"
      } else {
        return "A minute ago"
      }
    } else if (components.second >= 3) {
      return "\(components.second) seconds ago"
    } else {
      return "Just now"
    }
    
  }


}