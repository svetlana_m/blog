//
//  JYGHelper.swift
//  Firebase_initial_setup
//
//  Created by Svitlana Moiseyenko on 8/5/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation
import UIKit

final class UIHelper {
  
   // Configurate navigation Bar apearance
  class func configurationNavigationBar(navigationController: UINavigationController) {
    
    navigationController.setNavigationBarHidden(false, animated:true)
    navigationController.navigationBar.shadowImage = UIImage()
    navigationController.navigationBar.backgroundColor = UIColor.clearColor()
    navigationController.navigationBar.translucent = false
    navigationController.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
    navigationController.navigationBar.barTintColor = UIColor.whiteColor()
    let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor(named: UIColor.IOSColor.Orange)]
    navigationController.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
   
  }
  
}