//
//  Helper.swift
//  Firebase_initial_setup
//
//  Created by Svitlana Moiseyenko on 8/5/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import Foundation



final class PostHelper {
  
  static let sharedInstance = PostHelper()
  
  private var previousNumber: UInt32?
  let messages_array = ["Hi!", "Hello!", "Here is a new post", "What you think about...", "Recenlty, I published ...", "One interesting thing is ..."]

  func ganerateMesage() -> String {
    let random = randomNumber(UInt32(messages_array.count))
    return messages_array[random]
  }
  
  func randomNumber(max: UInt32) -> Int {
    var randomNumber = arc4random_uniform(max)
    while previousNumber == randomNumber {
      randomNumber = arc4random_uniform(max)
    }
    previousNumber = randomNumber
    return Int(randomNumber)
  }
  
}