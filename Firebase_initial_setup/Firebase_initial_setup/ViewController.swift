//
//  ViewController.swift
//  Firebase_initial_setup
//
//  Created by Svitlana Moiseyenko on 8/3/16.
//  Copyright © 2016 Svitlana Moiseyenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var imageView: UIImageView?
    @IBOutlet weak var postButton: UIButton! {
        didSet{
            postButton.tintColor = UIColor(named: UIColor.IOSColor.Orange)
        }
    }
    
    @IBOutlet weak var postView: UIView! {
        didSet{
            postView.backgroundColor = UIColor.whiteColor()
        }
    }
    
    @IBOutlet weak var postTable: UITableView! {
        didSet {
            postTable.backgroundColor = UIColor.whiteColor()
        }
    }
    
    private var observePostManager: ObservePostManager {
        return ObservePostManager.sharedInstance
    }
    
    @IBAction func onAddPost(sender: AnyObject) {
        addPost()
    }
    
    var posts = [Post]() {
        didSet {
            postTable.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIHelper.configurationNavigationBar(navigationController!)
        navigationItem.title = "Posts"
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        initPostObserve()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

/*
 This extension stores all methods 
 requires for working with posts
 */
extension ViewController {
    
    private func initPostObserve() {
        observePostManager.initPostsObserve { postsArray in
            self.posts = postsArray
        }
    }
    
    private func addPost() {
        let message = PostHelper.sharedInstance.ganerateMesage()
        observePostManager.createNewPost(message) { post in
            guard post != nil else {
                return
            }
        }
    }
    
    private func removePost(post: Post) {
        observePostManager.removePost(post)
    }
    
    private func updatePost(post: Post) {
        let isPublish = post.isPublished == 1 ? 0 : 1
        observePostManager.updatePostReadStatus(post, isPublish: isPublish)
    }

}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  posts.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(String(PostTableCell))! as! PostTableCell
        cell.selectionStyle = .None
        let postItem = posts[indexPath.row]
        cell.post = postItem
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete   {
            let post = posts[indexPath.row]
            removePost(post)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let postItem = posts[indexPath.row]
        updatePost(postItem)
        postTable.reloadData()
    }
}

